#简单规则及功能
*	通用controller回馈数据（注： controller 只写接口，不要出现任何别的代码 ，回馈数据也可组装成Result 处理controller回馈数据及controller日志可根据情况自行修改添加
*	通用异常（注： 所有无法处理的异常均抛出 只写接口 ）
*	通用字符编码处理（注： 前后台交互 ）
*	通用prop配置文件读取方法（配置文件 properties必须在 src/main/resource/ 目录下会自动读取，其他路径需自己添加至spring-application.xml
文件下的appConfigUtil 注：配置文件属性必须尽量避免重名（建议加文件前缀））
*	前后台交互code msg 在 ResultEnum 中维护
*	jdbc.properties 数据库连接配置文件
*	log4j.properties 日志配置文件
*	扫描mapper层规则 可自己看情况修改添加（mybatis配置文件下文件下）
*	通用mapper 需要在实体类上加对应注解
*	多数据源切换，需要配置AOP Service 切点，并且配置对应的数据源，通过注解切换

