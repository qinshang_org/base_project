package org.study.framework.basic.user.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Table;
@Table(name = "sbyg")
public class Sbyg {
	
	@Column(name = "ssss")
    private BigDecimal ssss;

	public Sbyg(){}
	public Sbyg(BigDecimal ssss){ this.ssss = ssss;}
    public BigDecimal getSsss() {
        return ssss;
    }

    public void setS(BigDecimal ssss) {
        this.ssss = ssss;
    }
}