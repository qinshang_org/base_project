package org.study.framework.basic.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.study.framework.basic.enums.ResultEnum;
import org.study.framework.basic.exception.BaseException;
import org.study.framework.util.ResultUtil;


/**
 * @ClassName: ExceptionHandle
 * @Description: 异常处理
 * @author qdt
 * @date 2018年9月11日 下午5:52:01
 */
@ControllerAdvice
public class ExceptionHandle {
	private static final Logger exceptionHandleLog = LoggerFactory.getLogger(ExceptionHandle.class);
	
	/** 
	 * @Title: handle 
	 * @Description: 处理异常 
	 * @param e
	 * @return Object
	 * @author qdt
	 * @date 2019年1月24日上午10:01:54
	 */ 
	@ExceptionHandler(value = Exception.class)
	@ResponseBody
    public Object handle(Exception e) {
		exceptionHandleLog.error("异常信息：",e);
		if (e instanceof BaseException) {
        	BaseException baseException = (BaseException) e;
            return ResultUtil.error(baseException.getResultEnum());
        }else {
            return ResultUtil.error(ResultEnum.UNKONW_ERROR);
        }
    }
}
