package org.study.framework.basic.user.entity;

import java.io.Serializable;
import java.util.Date;

public class RecordingTime implements Serializable{
    
	private static final long serialVersionUID = 3252970502662091718L;
	
	private Date recordingTimeId;

    public Date getRecordingTimeId() {
        return recordingTimeId;
    }

    public void setRecordingTimeId(Date recordingTimeId) {
        this.recordingTimeId = recordingTimeId;
    }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}