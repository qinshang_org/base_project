package org.study.framework.basic.user.entity;


import java.util.Date;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "state")
public class State implements Serializable{
	private static final long serialVersionUID = -765677485774243386L;
	
	@Id
	@Column(name = "state_id")
    private Integer stateId;
	@Column(name ="state_info")
    private String stateInfo;
	@Column(name ="state_update_time")
    private Date stateUpdateTime;

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public void setStateInfo(String stateInfo) {
        this.stateInfo = stateInfo ;
    }

    public Date getStateUpdateTime() {
        return stateUpdateTime;
    }

    public void setStateUpdateTime(Date stateUpdateTime) {
        this.stateUpdateTime = stateUpdateTime;
    }
    public static long getSerialversionuid() {
		return serialVersionUID;
	}
}