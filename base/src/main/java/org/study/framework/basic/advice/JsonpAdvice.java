package org.study.framework.basic.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

/**
 * @ClassName: JsonpAdvice
 * @Description: 响应JSONP请求
 * @author qdt
 * @date 2018年9月14日 下午1:41:22
 */
@ControllerAdvice
public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
	public JsonpAdvice() {
		super("callback");
	}
}
