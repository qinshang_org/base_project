package org.study.framework.basic.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.study.framework.basic.entity.Result;
import org.study.framework.basic.user.service.RecordingTimeService;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@RestController
@RequestMapping("/test22")
public class Test2Controller {
	@Autowired
	private RecordingTimeService recordingTimeService;
	@Autowired
	private Result asl;
	
	public Test2Controller(){
		System.out.println("Controller:"+this.hashCode());
	}
	@Autowired
	private ApplicationContext applicationContext;
	@RequestMapping("/test22")
	public Object test() throws Exception{
		//int a=1/0;
		System.out.println("sqlSessionFactory:"+applicationContext.getBean("sqlSessionFactory").hashCode());
		//System.out.println("annotationDrivenTransactionManager:"+applicationContext.getBean("annotationDrivenTransactionManager").hashCode());
		System.out.println("dynamicDataSource:"+applicationContext.getBean("dynamicDataSource").hashCode());
		System.out.println("basicDataSource:"+applicationContext.getBean("basicDataSource").hashCode());
		System.out.println("sysDataSource:"+applicationContext.getBean("sysDataSource").hashCode());
		System.out.println("Controller:"+this.hashCode());
		System.out.println("internalResourceViewResolver:"+applicationContext.getBean("internalResourceViewResolver").hashCode());
		return recordingTimeService.selectAll_2();
	}
}
