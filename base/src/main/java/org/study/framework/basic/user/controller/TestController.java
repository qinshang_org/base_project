package org.study.framework.basic.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.study.framework.basic.user.service.RecordingTimeService;


import com.github.pagehelper.PageHelper;

@Controller
@RequestMapping("/test")
public class TestController {
	@Autowired
	private RecordingTimeService recordingTimeService;
	
	
	
	
	@RequestMapping("/test")
	@ResponseBody
	public Object test() throws Exception{
		PageHelper.startPage(1, 20);
		return recordingTimeService.selectAll();
	}
	
	
	@RequestMapping("/test1")
	@ResponseBody
	public Object test1(String css) throws Exception{
		System.out.println(css);
		recordingTimeService.test1();
		return null;
	}
	
	@RequestMapping("/test2")
	@ResponseBody
	public void test2(String css) throws Exception{
		System.out.println(css);
		recordingTimeService.test2();
	}
}
