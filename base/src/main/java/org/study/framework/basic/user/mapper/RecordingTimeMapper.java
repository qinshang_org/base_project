package org.study.framework.basic.user.mapper;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.study.framework.basic.user.entity.RecordingTime;

@Repository
public interface RecordingTimeMapper {
    
    
    int deleteByPrimaryKey(Date recordingTimeId);

    int insert(RecordingTime record);

    List<RecordingTime> selectAll();
}