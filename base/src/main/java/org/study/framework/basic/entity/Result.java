package org.study.framework.basic.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName: Result
 * @Description: 响应前台传输数据
 * @author qdt
 * @date 2018年10月9日 上午9:32:07
 * @param <T>
 */
@Component
public class Result<T> implements Serializable {
	private static final long serialVersionUID = 2207384947814015128L;
	private Integer code;		// 编码
	private String msg;			// 信息
	private T data;				// 数据
	private long recordsTotal;	// 数据条数

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	@Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}

	
	
}
