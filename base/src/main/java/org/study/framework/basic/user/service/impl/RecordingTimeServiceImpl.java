package org.study.framework.basic.user.service.impl;



import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.study.framework.config.datasource.annotation.DataSource;
import org.study.framework.basic.user.entity.RecordingTime;
import org.study.framework.basic.user.entity.Sbyg;
import org.study.framework.basic.user.mapper.RecordingTimeMapper;
import org.study.framework.basic.user.mapper.SbygMapper;
import org.study.framework.basic.user.mapper.StateMapper;
import org.study.framework.basic.user.service.RecordingTimeService;


@Service
@DataSource("basic")
public class RecordingTimeServiceImpl  implements RecordingTimeService {
	@Autowired
	private RecordingTimeMapper rtm;
	@Autowired
	private StateMapper sm;
	
	@Autowired
	private SbygMapper sbmp;
	
	//事务级别：读写提交分离 ->更好的并发性
	//事务传播级别：有则延续，无则创建
	@Override
	@DataSource("basic")
	//@Transactional(isolation=Isolation.READ_COMMITTED ,propagation=Propagation.REQUIRED)
	public List<RecordingTime> selectAll() throws Exception {
		return rtm.selectAll();
	}
	//@DataSource("basic")
	//@Transactional(isolation=Isolation.READ_COMMITTED ,propagation=Propagation.REQUIRED)
	public List selectAll_2() throws Exception {
		
		return sm.selectAll();
	}
	@Override
	@DataSource("test1")
	@Transactional(isolation=Isolation.READ_COMMITTED ,propagation=Propagation.REQUIRED , rollbackFor = Exception.class )
	public void test1() throws Exception {
		sbmp.insert(new Sbyg(BigDecimal.valueOf(1)));
		
		sbmp.insert(new Sbyg(BigDecimal.valueOf(2)));
		//throw new Exception();
		
	}
	@DataSource("test2")
	@Transactional( isolation=Isolation.READ_COMMITTED ,propagation=Propagation.REQUIRED ,rollbackFor = Exception.class)
	public void test2() throws Exception {
		sbmp.insert(new Sbyg(BigDecimal.valueOf(1)));
		
		sbmp.insert(new Sbyg(BigDecimal.valueOf(2)));
		throw new Exception();
	}
	
}
