package org.study.framework.config.springmvc;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * @ClassName: SpringMVCConfig
 * @Description: springMVC配置类
 * @author qdt
 * @date 2018年9月29日 上午11:03:43
 */
//定义springMVC扫描的包
@ComponentScan(value = "org.study.framework.*" ,
	includeFilters = {
		@Filter(
			type = FilterType.ANNOTATION,
			value = {Controller.class}
		)
	}
)
//启用spring MVC配置
@EnableWebMvc
public class SpringMVCConfig {
	private static final Logger springMVCConfigLog = LoggerFactory.getLogger(SpringMVCConfig.class);
	/** 
	 * @Title: initViewResolver 
	 * @Description: 配置视图解析器 
	 * @return ViewResolver
	 * @author qdt
	 * @date 2018年9月29日上午11:09:16
	 */ 
	@Bean(name = "internalResourceViewResolver")
	public ViewResolver initViewResolver(){
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setPrefix("/html/");
		internalResourceViewResolver.setSuffix(".html");
		springMVCConfigLog.error("装配ViewResolver,其hashCode为{}",internalResourceViewResolver.hashCode());
		return internalResourceViewResolver;
	}
	
	/** 
	 * @Title: initRequestMappingHandlerAdapter 
	 * @Description: 加载RequestMappingHandlerAdapter并初始化 Http的JSON转换器
	 * @return HandlerAdapter
	 * @author qdt
	 * @date 2018年9月29日上午11:18:47
	 */ 
	@Bean(name = "requestMappingHandlerAdapter")
	public HandlerAdapter initRequestMappingHandlerAdapter(){
		//创建适配器
		RequestMappingHandlerAdapter requestMappingHandlerAdapter = new RequestMappingHandlerAdapter();
		//HTTP JSON转换器
		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = 
				new MappingJackson2HttpMessageConverter();
		//接受转换的消息类型
		MediaType mediaType = MediaType.APPLICATION_JSON_UTF8;
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(mediaType);
		//加入转换器
		mappingJackson2HttpMessageConverter.setSupportedMediaTypes(mediaTypeList);
		//加入适配器
		requestMappingHandlerAdapter.getMessageConverters().add(mappingJackson2HttpMessageConverter);
		springMVCConfigLog.error("装配HandlerAdapter,其hashCode为{}",requestMappingHandlerAdapter.hashCode());
		return requestMappingHandlerAdapter;
	}
}
