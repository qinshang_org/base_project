package org.study.framework.config.spring;


import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import org.study.framework.config.datasource.DynamicDataSource;
import org.study.framework.util.AppConfigUtil;

import com.github.pagehelper.PageHelper;

import tk.mybatis.spring.mapper.MapperScannerConfigurer;


/**
 * @ClassName: SpringApplicationConfig
 * @Description: spring配置类
 * @author qdt
 * @date 2018年9月29日 上午10:54:59
 */
@ComponentScan(value = "org.study.framework.*" ,
//	includeFilters = {
//		@Filter(
//			type = FilterType.ANNOTATION,
//			value = {Service.class,Component.class}
//		)
//	}
	excludeFilters={
		@Filter(
				type = FilterType.ANNOTATION,
				value = {Controller.class}
			)
	}
)

//开启事务驱动管理器->启用注解配置事务
@EnableTransactionManagement
//@ImportResource({"classpath:spring-application.xml"})
@Import(SpringConfig.class)
public class SpringApplicationConfig /*implements TransactionManagementConfigurer */{
	
	private static final Logger springApplicationConfigLog = LoggerFactory.getLogger(SpringApplicationConfig.class);
	
	@Bean(name = "sqlSessionFactory")
	public SqlSessionFactoryBean initSqlSessionFactory(
			@Autowired
			@Qualifier("dynamicDataSource")
			DataSource dynamicDataSource   
			){
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dynamicDataSource);
		//Resource mybatisCofigResource = new ClassPathResource("mybatis/mybatis-config.xml");
		//sqlSessionFactory.setConfigLocation(mybatisCofigResource);
		//sqlSessionFactory.
		//分页插件设置
		sqlSessionFactory.setPlugins(new Interceptor[] {new PageHelper()});
		springApplicationConfigLog.debug("装配SqlSessionFactoryBean{}",sqlSessionFactory.hashCode());
		return sqlSessionFactory;
	}
	
	/** 
	 * @Title: initMapperScannerConfigurer 
	 * @Description: 自动扫描Mybatis Mapper接口 
	 * @return MapperScannerConfigurer
	 * @author qdt
	 * @date 2018年9月29日上午10:50:35
	 */ 
	@Bean
	public MapperScannerConfigurer initMapperScannerConfigurer(){
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		mapperScannerConfigurer.setBasePackage("org.study.framework.*");
		mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
		mapperScannerConfigurer.setAnnotationClass(Repository.class);
		Properties mybatisMapperProperties = new Properties();
		//tk.mybatis.spring.mapper.
		mybatisMapperProperties.setProperty( "mappers" , "tk.mybatis.mapper.common.Mapper" );
		mapperScannerConfigurer.setProperties(mybatisMapperProperties);
		springApplicationConfigLog.debug("装配MapperScannerConfigurer{}",mapperScannerConfigurer.hashCode());
		return mapperScannerConfigurer;
	}
	/**
	 * 声明事务管理器
     * 
     */
    @Bean
    public DataSourceTransactionManager getDataSourceTransactionManager(
    		@Autowired
			@Qualifier("dynamicDataSource")
			DataSource dynamicDataSource 
    		) {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dynamicDataSource);
        springApplicationConfigLog.debug("装配DataSourceTransactionManager{}",dataSourceTransactionManager.hashCode());
        return dataSourceTransactionManager;
    }

	/**
	 * @Title: annotationDrivenTransactionManager
	 * @Description: 实现事务接口 在使用注解时产生数据库事务
	 * @return 
	 * @see org.springframework.transaction.annotation.TransactionManagementConfigurer#annotationDrivenTransactionManager() 
	 
	@Override
	@Bean(name = "annotationDrivenTransactionManager")
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(sysDataSource);
		
		logger.info("dataSourceTransactionManager.hashcode={}",dataSourceTransactionManager.hashCode());
		return dataSourceTransactionManager;
	}*/
	/*
	@Bean(name = "sqlSessionTemplate" , destroyMethod="close")
	@Scope("prototype")
	public SqlSessionTemplate sqlSessionTemplate(
			@Autowired
			@Qualifier("sqlSessionFactory")
			SqlSessionFactoryBean sqlSessionFactory  
			) {
		SqlSessionTemplate sqlSessionTemplate = null;
		try {
			sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory.getObject() ,ExecutorType.BATCH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("sqlSessionTemplate.hashcode={}",sqlSessionTemplate.hashCode());
		logger.info("sqlSessionTemplate.sqlSessionFactory.hashcode={}",sqlSessionFactory.hashCode());
		return sqlSessionTemplate;
	}*/
	
	@Bean(name = "dynamicDataSource")
	public DataSource initDynamicDataSource(){

		Map<String, Object> ctxPropertiesMap = AppConfigUtil.getContextPropertyMap();
		
		Set<String> keySet = ctxPropertiesMap.keySet();
		
		Set<String> dataSourceKey = new HashSet<String>();
		for (String key : keySet) {
			if(key.startsWith("jdbc.dataSource")) {
				dataSourceKey.add(key.split("\\.")[2]);
			}
		}
		
		Map<Object , Object> dataSourcesMap = new HashMap<Object , Object>();
		
		
		Properties dataSourceProperties = new Properties() ;
		//初始化连接大小
		dataSourceProperties.setProperty( "initialSize" , getConfigString("jdbc.initialSize") );
		//连接池最大数量
		dataSourceProperties.setProperty( "maxActive" , getConfigString("jdbc.maxActive") );
		//连接池最大空闲 
		dataSourceProperties.setProperty( "maxIdle" , getConfigString("jdbc.maxIdle") );
		//连接池最小空闲 
		dataSourceProperties.setProperty( "minIdle" , getConfigString("jdbc.minIdle") );
		//获取连接最大等待时间
		dataSourceProperties.setProperty( "maxWait" , getConfigString("jdbc.maxWait") );
		/*
		dataSourceProperties.setProperty( "timeBetweenEvictionRunsMillis" , getConfigString("jdbc.timeBetweenEvictionRunsMillis") );
		dataSourceProperties.setProperty( "minEvictableIdleTimeMillis" , getConfigString("jdbc.minEvictableIdleTimeMillis") );
		dataSourceProperties.setProperty( "validationQuery" , getConfigString("jdbc.validationQuery") );
		dataSourceProperties.setProperty( "testWhileIdle" , getConfigString("jdbc.testWhileIdle") );
		dataSourceProperties.setProperty( "testOnBorrow" , getConfigString("jdbc.testOnBorrow") );
		 */
		
		DataSource dataSource = null;
		//
		for (String key : dataSourceKey) {
			dataSourceProperties.setProperty( "driverClassName" , 
					(String)ctxPropertiesMap.get(
							"jdbc.dataSource.#dataSourceName.driverClassName".replace("#dataSourceName", key)
							)
					);
			dataSourceProperties.setProperty( "url" , 
					(String)ctxPropertiesMap.get(
							"jdbc.dataSource.#dataSourceName.url".replace("#dataSourceName", key)
							)
					);
			dataSourceProperties.setProperty( "username" , 
					(String)ctxPropertiesMap.get(
							"jdbc.dataSource.#dataSourceName.username".replace("#dataSourceName", key)
							)
					);
			dataSourceProperties.setProperty( "password" , 
					(String)ctxPropertiesMap.get(
							"jdbc.dataSource.#dataSourceName.password".replace("#dataSourceName", key)
							)
					);
			try {
				//org.apache.commons.dbcp连接池
				springApplicationConfigLog.debug("装配数据库连接池{}",key);
				dataSource = BasicDataSourceFactory.createDataSource(dataSourceProperties);
				dataSourcesMap.put(key, dataSource);
			} catch (Exception e) {
				springApplicationConfigLog.debug(key+"数据库连接池装配失败{}",e);
			}
		}
		
		DynamicDataSource dynamicDataSource =new DynamicDataSource();
		
		dynamicDataSource.setTargetDataSources(dataSourcesMap);
		dynamicDataSource.setDefaultTargetDataSource(dataSourcesMap.get(getConfigString("jdbc.default.dataSource")));
		springApplicationConfigLog.debug("装配DataSource,其hashCode为{}",dynamicDataSource.hashCode());
	
		return dynamicDataSource;
	}
	
	private String getConfigString(String name){
		return AppConfigUtil.getContextPropertyString(name);
	}
	
	
	
	/** 
	 * @Title: initAppConfigUtil 
	 * @Description: 配置文件读取 
	 * @return
	 * @throws IOException AppConfigUtil
	 * @author qdt
	 * @date 2018年10月8日下午4:08:03
	 */ 
	@Bean(name = "appConfigUtil")
	public static AppConfigUtil initAppConfigUtil(){
		AppConfigUtil appConfigUtil = new AppConfigUtil();
		PathMatchingResourcePatternResolver pmrpr = new  PathMatchingResourcePatternResolver();
		Resource[] resoures=null;
		try {
			springApplicationConfigLog.debug("装配AppConfigUtil,其hashCode为{}",appConfigUtil.hashCode());
			resoures = pmrpr.getResources("dev/*.properties");
		} catch (IOException e) {
			springApplicationConfigLog.error("装配AppConfigUti失败{}", e );
		}
		appConfigUtil.setLocations(resoures );
		return appConfigUtil;
	}
}
