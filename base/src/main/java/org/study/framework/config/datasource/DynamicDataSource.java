package org.study.framework.config.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @ClassName: DynamicAbstractRoutingDataSource
 * @Description: 切换数据库
 * @author qdt
 * @date 2018年9月17日 下午5:04:31
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
	@Override
	protected Object determineCurrentLookupKey() {
		// 从自定义的位置获取数据源标识
		return DynamicDataSourceHolder.getDataSource();
	}
	
	
}