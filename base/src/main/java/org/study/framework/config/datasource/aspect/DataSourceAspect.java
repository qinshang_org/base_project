package org.study.framework.config.datasource.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.study.framework.config.datasource.DynamicDataSourceHolder;
import org.study.framework.config.datasource.annotation.DataSource;
import org.study.framework.util.AppConfigUtil;

import java.lang.reflect.Method;

import org.study.framework.config.constant.PointcutConfig;

/**
 * @ClassName: DataSourceAspect
 * @Description: 数据源切换AOP操作
 * @author qdt
 * @date 2018年9月17日 下午5:00:33
 */
@Aspect
@Component
@Order(0)
public class DataSourceAspect {
	private static final Logger dataSourceAspectLog = LoggerFactory.getLogger(DataSourceAspect.class);
	
	/** 
	 * @Title: setDataSource 
	 * @Description: 数据源切换切入点(以@DataSource注解为切入点)  void
	 * @author qdt
	 * @date 2018年9月30日下午2:23:28
	 */ 
	@Pointcut(PointcutConfig.DATA_SOURCE_SERVICE_POINTCUT)
	public void setDataSource() {}

	/**
	 * @Title: 拦截目标方法
	 * @Description: 获取由@DataSource指定的数据源标识，设置到线程存储中以便切换数据源
	 * @param point
	 * @throws Exception
	 * @author qdt
	 * @date 2018年9月17日下午5:03:43
	 */
	@Before("setDataSource()")
	public void intercept(JoinPoint point) throws Exception {
		Class<?> target = point.getTarget().getClass();
		MethodSignature signature = (MethodSignature) point.getSignature();
		// 默认使用目标类型的注解，如果没有则使用其实现接口的注解
		for (Class<?> clazz : target.getInterfaces()) {
			resolveDataSource(clazz, signature.getMethod());
		}
		resolveDataSource(target, signature.getMethod());
	}
	
	/** 
	 * @Title: after 
	 * @Description: 在执行完操作后将数据源切换回默认数据源 
	 * @throws Exception
	 * @author qdt
	 * @date 2018年9月30日下午2:25:18
	 */ 
	@After("setDataSource()")
	public void after() throws Exception {
		String dataSourceValue = (String) AppConfigUtil.getContextProperty("dataSource");
		if(!DynamicDataSourceHolder.getDataSource().equals(dataSourceValue)) {
			dataSourceAspectLog.debug("切换回默认的数据库{}", dataSourceValue);
			DynamicDataSourceHolder.setDataSource(dataSourceValue);
		}
			
	}
	/**
	 * @Title: resolveDataSource
	 * @Description: 提取目标对象方法注解和类型注解中的数据源标识，按照等级依次覆盖后判断当前数据源是否与要切换的数据源相同，不同便切换
	 * @param clazz
	 * @param method
	 * @author qdt
	 * @date 2018年9月17日下午5:03:27
	 */
	private void resolveDataSource(Class<?> clazz, Method method) {
		try {
			Class<?>[] types = method.getParameterTypes();
			String soureValue = null ;
			if (clazz.isAnnotationPresent(DataSource.class)) {
				soureValue = clazz.getAnnotation(DataSource.class).value();
			}
			Method m = clazz.getMethod(method.getName(), types);
			if (m != null && m.isAnnotationPresent(DataSource.class)) {
				soureValue = m.getAnnotation(DataSource.class).value();
			}
			String oldSoureValue = DynamicDataSourceHolder.getDataSource();
			if(soureValue !=null && (!soureValue.equals(oldSoureValue)) ){
				dataSourceAspectLog.debug("原数据库{}", oldSoureValue);
				dataSourceAspectLog.debug("切换后的数据库{}", soureValue);
				DynamicDataSourceHolder.setDataSource(soureValue);
			}
			/*
			Class<?>[] types = method.getParameterTypes();
			// 默认使用类型注解
			if (clazz.isAnnotationPresent(DataSource.class)) {
				DataSource source = clazz.getAnnotation(DataSource.class);
				DynamicDataSourceHolder.setDataSource(source.value());
			}
			// 方法注解可以覆盖类型注解
			Method m = clazz.getMethod(method.getName(), types);
			if (m != null && m.isAnnotationPresent(DataSource.class)) {
				DataSource source = m.getAnnotation(DataSource.class);
				DynamicDataSourceHolder.setDataSource(source.value());
			}
			*/
			
		} catch (Exception e) {
			dataSourceAspectLog.error("数据库切换出错：", e);
		}
	}
}
