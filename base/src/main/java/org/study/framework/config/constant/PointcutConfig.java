package org.study.framework.config.constant;

import org.springframework.stereotype.Component;


/**
 * @ClassName: PointcutConfig
 * @Description: AOP切入点配置
 * @author qdt
 * @date 2018年9月17日 上午9:44:45
 */
@Component
public class PointcutConfig {
	
	
	/**
	 * @Fields CONTROLLER_POINTCUT : Controller切入点配置，用于处理Conreoller方法的返回值及对应日志
	 */
	public final static String LOG_CONTROLLER_POINTCUT = "@within(org.springframework.web.bind.annotation.RequestMapping) || @annotation(org.springframework.web.bind.annotation.RequestMapping)";
	
	/**
	 * @Fields CONTROLLER_POINTCUT : 数据库切换切入点配置，用于处理service层数据库的切换
	 */
	public final static String DATA_SOURCE_SERVICE_POINTCUT = "@within(org.study.framework.config.datasource.annotation.DataSource)|| @annotation(org.study.framework.config.datasource.annotation.DataSource)";
}
