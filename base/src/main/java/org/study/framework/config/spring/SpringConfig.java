package org.study.framework.config.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @ClassName: SpringApplicationConfig
 * @Description: spring配置类
 * @author qdt
 * @date 2018年9月29日 上午10:54:59
 */
@Configuration
//启用AOP
@EnableAspectJAutoProxy
public class SpringConfig {}
