package org.study.framework.config.datasource;


/**
 * @ClassName: DynamicDataSourceHolder
 * @Description: 数据库切换
 * @author qdt
 * @date 2018年9月17日 下午5:07:20
 */
public class DynamicDataSourceHolder {
	/**
	* 注意：数据源标识保存在线程变量中，避免多线程操作数据源时互相干扰
	*/
	private static final ThreadLocal<String> THREAD_DATA_SOURCE = new ThreadLocal<String>();

	public static String getDataSource() {
		return THREAD_DATA_SOURCE.get();
	}

	public static void setDataSource(String dataSource) {
		THREAD_DATA_SOURCE.set(dataSource);
	}

	public static void clearDataSource() {
		THREAD_DATA_SOURCE.remove();
	}
}
