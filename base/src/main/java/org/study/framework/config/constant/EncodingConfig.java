package org.study.framework.config.constant;

import org.springframework.stereotype.Component;
/**
 * @ClassName: EncodingConfig
 * @Description: 编码格式通用常量池
 * @author qdt
 * @date 2018年10月8日 下午4:50:27
 */
@Component
public class EncodingConfig {
	/**
	 * @Fields characterEncoding : 设置的编码
	 */
	public final static String characterEncoding="utf-8";
	/**
	 * @Fields charEncoding : 默认的编码
	 */
	public final static String charEncoding="iso8859-1";
	
	
	
}
