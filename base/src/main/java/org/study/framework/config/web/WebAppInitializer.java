package org.study.framework.config.web;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import org.study.framework.config.spring.SpringApplicationConfig;
import org.study.framework.config.springmvc.SpringMVCConfig;


/**
 * @ClassName: WebAppInitializer
 * @Description: Servlet配置,加载spring，springMVC配置类等等 -> 类似于web.xml
 * @author qdt
 * @date 2018年9月29日 上午10:22:05
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	private static final Logger webAppInitializerLog = LoggerFactory.getLogger(WebAppInitializer.class);
    /**
     * @Title: getRootConfigClasses
     * @Description: 配置Spring IOC环境配置
     * @return 
     * @see org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getRootConfigClasses() 
     */
    protected Class<?>[] getRootConfigClasses() {
    	//配置Spring配置类
    	webAppInitializerLog.debug("配置Spring配置类{}",SpringApplicationConfig.class.getName());
        return new Class<?>[]{SpringApplicationConfig.class};
    }
    /**
     * @Title: getServletConfigClasses
     * @Description: 配置Spring Web上下文配置类//DispatcherServlet配置//加载SpringMVC配置类
     * @return 
     * @see org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getServletConfigClasses() 
     */
    protected Class<?>[] getServletConfigClasses() {
    	//配置Spring Web上下文配置类//DispatcherServlet配置//加载SpringMVC配置类
    	webAppInitializerLog.debug("配置Spring Web上下文配置类,DispatcherServlet配置,加载SpringMVC配置类{}",SpringMVCConfig.class.getName());
        return new Class<?>[]{SpringMVCConfig.class};
    }

    
    /**
     * @Title: getServletMappings
     * @Description: 配置在什么路径下使用Spring的DispatcherServlet。等价于给DispatchServlet指定对应的url-mapping
     * @return 
     * @see org.springframework.web.servlet.support.AbstractDispatcherServletInitializer#getServletMappings() 
     */
    @Override
    protected String[] getServletMappings() {
    	//配置在什么路径下使用Spring的DispatcherServlet。等价于给DispatchServlet指定对应的url-mapping
    	webAppInitializerLog.debug("配置在什么路径下使用Spring的DispatcherServlet，等价于给DispatchServlet指定对应的url-mapping");
        return new String[]{"/"};
    }
    
    
    /**
     * @Title: customizeRegistration
     * @Description: 文件上传配置
     * @param dynamic 
     * @see org.springframework.web.servlet.support.AbstractDispatcherServletInitializer#customizeRegistration(javax.servlet.ServletRegistration.Dynamic) 
     */
    @Override
    protected void customizeRegistration(Dynamic dynamic) {
    	
        //文件上传路径
    	String filePath = "e:/mvc/uploads";
    	webAppInitializerLog.debug("文件上传配置,文件上传临时位置{}",filePath);
    	//上传文件最大限制为10M
    	Long singleMax = (long)(10*Math.pow(2, 20));
    	//所有数据上传限制为20M
    	Long totalMax = (long)(20*Math.pow(2, 20));
    	
    	dynamic.setMultipartConfig(new MultipartConfigElement(filePath , singleMax , totalMax , 0));
    }

    /**
     * 配置与Spring相关的Filter。这里规定Spring MVC的编码为UTF-8*** 无法解决get请求乱码问题
     
    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return new Filter[]{characterEncodingFilter};
    }*/
}
