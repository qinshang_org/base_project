package org.study.framework.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.study.framework.basic.user.controller.TestController;
import org.study.framework.config.spring.SpringApplicationConfig;
import org.study.framework.config.springmvc.SpringMVCConfig;
import org.study.framework.config.web.WebAppInitializer;

/*
 * 纯注解方式整合Junit单元测试框架测试类
 */
@RunWith(value=SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={SpringApplicationConfig.class,SpringMVCConfig.class})//需要注意此处，将加载配置文件的注解换成加载配置类的注解
//@ContextConfiguration(classes={WebAppInitializer.class,SpringApplicationConfig.class,SpringMVCConfig.class})//需要注意此处，将加载配置文件的注解换成加载配置类的注解
public class Demo2 {
	@Autowired
    private TestController userController;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }
	@Test
	public void test() {
		System.out.println("11111111111111");
	}
}
